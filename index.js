function cuadrado(num) {
    return num * num;
}


function factorial(num) {
    //chequeamos que sea un entero
    if (Number.isInteger(num)){
        //declaramos valor inicial para tener algo con qué ir multiplicando los enteros a iterar
        fact = 1;
        // iteramos por el "num" y sus enteros inferiores, y los multiplicamos
        for (var i=num; i>0; i--) {
            fact *= i;
        }
        return fact;
    } else {
        console.log("number is not an integer");
    }
}


function circulo(radio) {
    pi = 3.1416;
    return radio * radio * pi;
}


function login(user, password) {

    //declaramos un array con la base de datos
    data = [["user1", "pass1"], ["user2", "pass2"], ["user3", "pass3"]];
    //iteramos por los elementos del array
    for (var i = 0; i < data.length; i++) {
        //iteramos dentro de cada elemento del array
        for (var j = 0; j < data[i].length; j++) {
            // chequeamos si hay coincidencia
            if (user === data[i][j] && password === data[i][j+1]){
                return true;            
            }          
        }
    }
    // si no hay coincidencia, devolvemos false
    return false;
}